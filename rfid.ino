#include <SoftwareSerial.h>

// RFID stuff
SoftwareSerial ssrfid(D4,8); 

const int RFID_BUFFER_SIZE = 12;

uint8_t rfid_buffer[RFID_BUFFER_SIZE];

uint8_t rfid_tags[10][RFID_BUFFER_SIZE] = 
{{54,53,48,48,53,65,68,52,54,70,56,52},
{54,53,48,48,53,56,57,67,50,65,56,66},
{54,52,48,48,65,69,56,56,53,52,49,54},
{54,52,48,48,65,69,69,67,54,50,52,52},
{54,53,48,48,53,56,55,53,49,54,53,69},
{54,67,48,48,65,48,65,50,69,53,56,66},
{54,53,48,48,53,56,54,55,66,65,69,48},
{54,52,48,48,65,69,56,49,69,55,65,67},
{54,53,48,48,53,56,53,51,55,67,49,50},
{54,53,48,48,53,65,68,65,55,69,57,66}};

void initRFID() {
  ssrfid.begin(9600);
  ssrfid.listen();
}

int readRFIDTag() {
  if (ssrfid.available() <= 0) // No data available
    return 0;
    
  bool complete = false;
  
  int ssvalue = ssrfid.read();
  if (ssvalue != 2) // Got nothing
    return 0;

  int buffer_index = 0;
  while (buffer_index <= RFID_BUFFER_SIZE) {
    while (ssrfid.available() <= 0); // Wait until there's data available
    
    ssvalue = ssrfid.read();
    
    if (ssvalue == 2) { // Start of tag again I guess?
      buffer_index = 0;
    } else if (ssvalue != 3) { // Not done reading yet
      if (buffer_index == RFID_BUFFER_SIZE) // TMI!
        return 255;
      
      rfid_buffer[buffer_index++] = ssvalue;
    } else if (buffer_index == RFID_BUFFER_SIZE) { // Done reading!
      bool matches;
      for (int id = 0; id < 10; ++id) {
        matches = true;

        // Check if the tag ID matches any known ones
        for (buffer_index = RFID_BUFFER_SIZE-1; buffer_index > 0; --buffer_index)
          if (rfid_buffer[buffer_index] != rfid_tags[id][buffer_index])
            matches = false;

        // Is the tag known? Return its index!
        if (matches)
          return id + 1;
      }
    } else { // Not enough information!
      return -1;
    }
  }
  
  return -1;
}

/*void setup() {
  Serial.begin(115200);

  delay(500);
  
  initRFID();
}

void loop() {
  uint8_t id = readTag();

  if (id != 0 && id != 255) {
    Serial.println(id);
  }
  
  delay(10);
}*/
