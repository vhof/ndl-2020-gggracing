#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
#import cwiid  # install on linux, requirements libcwiid1
from car_module import *
#from Path_finder import *
import time
import struct
import threading
import socket
from gamepad import *

# Car socket
car_socket = None

# Address of car
#device_ip = '192.168.0.124'
device_ip = '192.168.0.108'
#device_ip = '192.168.43.163'
#device_ip = '192.168.1.55'
device_port = 3991

# CONTROLLER SETTINGS
# Speed of the car, up to 100
car_speed = 35
# Maximum turning time for the car, if it hasn't reached the turn_z value below yet.
turn_time = 3000
# Amount of rotation required for a 90-degree (or different, if desired) angle.
# This may have to be adjusted depending on the friction on a surface
turn_z = 260000

# RGB ratio (Green/Red, Blue/Green) values for different actions detected by the color sensor
# By using ratios rather than absolute RGB values,
#  these values can keep working independent of outside light
# The values we have here correspond to post-it notes we used in our project
action_colors = [
  0.87, 0.66, # Forwards, green
  0.59, 0.59, # Backwards, yellow
  0.98, 1.06, # Left, blue
  0.54, 0.92, # Right, pink
  0.35, 0.90 # Stop, red
];
# Sensitivity for color detection (|delta green/red ratio| + |delta blue/green ratio| < color_sensitivity)
color_sensitivity = 0.2



# Do not edit, these are values modified by the program
follows_color = False
left = 0
right = 0

# Thread state
running = True


# Thread that handles button pressing
def handle_buttons():
    global running
    global follows_color
    global left
    global right
    
    while running:
        action = read_gamepad_action()
        if action is None:
          running = False
          break
        
        stick, type, value = action
        
        if (stick and (type == STICK_LT or type == STICK_RT or type == STICK_LX)) or (not stick and type == KEY_A):
          # Steering with left stick
          steer = stickState[STICK_LX]
          
          # Right trigger = forward speed
          # Left trigger = backward speed
          # They cancel each other out
          left =  int((stickState[STICK_RT] - stickState[STICK_LT]) / 2 * car_speed)
          right = left
          
          if keyState[KEY_A]:
            # If A is held, the car rotates around its own axis
            #  (by inverting one side of wheels)
            if steer <= -0.5:
              left = -left
            elif steer >= 0.5:
              right = -right
          else:
            # If A is not held, the car steers by:
            #  slowing down the steering side
            #  speeding up the opposite side
            # to make for more natural-looking steering
            if steer <= -0.5:
              right = int(right * 1.5)
              left = int(left * 0.25)
            elif steer >= 0.5:
              left = int(left * 1.5)
              right = int(right * 0.25)
        
        # Start button toggles color following
        if not stick and type == KEY_START and not value:
          follows_color = not follows_color
          set_follows_color(follows_color)
        
        # Select button retrieves the RGB value from the color sensor
        if not stick and type == KEY_SELECT and not value:
          car_socket.send(struct.pack('b', 2))
          print("Receiving color")
          time.sleep(0.25)
          try:
            response = struct.unpack('<b3H', car_socket.recv(7, socket.MSG_DONTWAIT))[1:]
            ratio = [ response[1] / response[0], response[2] / response[1] ]
            print("Current color: " + str(response) + ", ratio: " + str(ratio))
          except BlockingIOError:
            print("Failed to receive color, try again or consider restarting the program")
            pass


# Thread that sends motor data to the car
def spam_car():
  global running
  global left
  global right
  
  lastLeft, lastRight = 0, 0
  connected = True
  while running:
    l = left
    r = right
    # Only send data when it has changed
    if l != lastLeft or r != lastRight:
      if not send_to_car(l, r):
        if connected:
          connected = False
          print("Connection to car lost")
      else:
        lastLeft = l
        lastRight = r
        if not connected:
          connected = True
          print("Connection re-established")
      
    time.sleep(0.05)
  
  gamepad_close()


# Established car connection
def connect_to_car():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect((device_ip, device_port))
    return sock


# Sends (left, right) motor data to the car
def send_to_car(left, right):
    try:
      car_socket.send(struct.pack('iib', left, right, 0))
      return True
    except ConnectionRefusedError:
      return False


# Sends following state to the car
def set_follows_color(follow):
    global action_colors
    
    if follow:
        # If the car starts following, information for this is also sent
        car_socket.send(struct.pack('<10H4ibb', *[int(i * 100) for i in action_colors], int(color_sensitivity * 100), car_speed, turn_time, turn_z, 1, 1))
    else:
        car_socket.send(struct.pack('bb', 0, 1))

if __name__ == "__main__":
    # connect to car
    car_socket = connect_to_car()

    # Spawn separate threads for controller and motor duty handling
    threading.Thread(target=gamepad_thread).start()
    threading.Thread(target=spam_car).start()
    # Handle button presses on the current thread
    handle_buttons()
