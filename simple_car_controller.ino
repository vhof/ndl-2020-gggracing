#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include "Adafruit_TCS34725.h"


#define PORT 3991
#define EFFECT_TIME 5000

// Stuff for color sensor
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);

// Wifi stuff
//char* ssid = "I know where your house lives";
//char* pass = "atHomeRight?";
char* ssid = "TP-Links";
char* pass = "OfTochRechts";
//char* ssid = "NDL_24G";
//char* pass = "RT-AC66U";

WiFiUDP udp;
char incomingPacket[255];

// Motor stuff
LOLIN_I2C_MOTOR motor1(0x30); //I2C address 0x30
LOLIN_I2C_MOTOR motor2(0x31); //using customize I2C address
float leftDuty = 0, rightDuty = 0, lastLeft = 0, lastRight = 0, leftStatus = 0, rightStatus = 0;


// RFID effect stuff
enum { NO_EFFECT, OIL, REVERSE, GLUE, SUGAR, SLIME } activeEffect = NO_EFFECT;
unsigned long effectEnd = 0, lastTimeLeft;
long oilAcc = 0;


// Color following stuff
bool followsColor = false;
uint16_t sRed, sGreen, sBlue;
int colorCount = 5;
float actionColorSensitivity = 0.1;
float actionColors[5][2] = {
  { 0, 0 }, // Forward
  { 0, 0 }, // Backward
  { 0, 0 }, // Left
  { 0, 0 }, // Right
  { 0, 0 }  // Stop
};
int turnTime = 1000, turnZ = 250000, turnDirection = -1;
long turnEnd = 0;
int scannedColor = -1, activeColor = -1, colorCounter = 0;
int gyroZ = 0;
int carSpeed = 50;
long loopTime = 0;


// Takes current duty, applies any effects and then updates motors
void applyMotor() {
  float left = leftDuty, right = rightDuty;
  if (millis() < effectEnd) {
    float temp;
    switch (activeEffect) {
      case OIL:
        left += ((oilAcc) % 20) - 10;
        oilAcc *= 686969; // Multiply by a prime number, to create some pseudo-randomness
        right += ((oilAcc) % 20) - 10;
        break;
      case REVERSE:
        temp = -left;
        left = -right;
        right = temp;
        break;
      case GLUE:
        return; // Don't change motor duty
      case SUGAR:
        left *= 2;
        right *= 2;
        break;
      case SLIME:
        left *= 0.75;
        right *= 0.75;
        break;
    }
  }
  if (left == lastLeft && right == lastRight)
    return;


  // Only change motor states if they need to change
  if ((leftStatus <= 0 && left > 0) || (leftStatus >= 0 && left < 0)) {
    if (left > 0) {
      motor2.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CW);
      motor2.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CCW);
      leftStatus = left;
    } else if (left < 0) {
      motor2.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);
      motor2.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CW);
      leftStatus = left;
    }
  }

  // Same here
  if ((rightStatus <= 0 && right > 0) || (rightStatus >= 0 && right < 0)) {
    if (right > 0) {
      motor1.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);
      motor1.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CW);
      rightStatus = right;
    } else if (right < 0) {
      motor1.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CW);
      motor1.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CCW);
      rightStatus = right;
    }
  }

  // Apply duty
  int dLeft = abs(left), dRight = abs(right);

  if (left != lastLeft) {
    if (right != lastRight) {
      motor1.changeDuty(MOTOR_CH_BOTH, dRight);
      motor2.changeDuty(MOTOR_CH_BOTH, dLeft);
    } else {
      motor2.changeDuty(MOTOR_CH_BOTH, dLeft);
    }
  } else if (right != lastRight) {
    motor1.changeDuty(MOTOR_CH_BOTH, dRight);
  }
  lastLeft = left;
  lastRight = right;
}

// Sets motor values and updates
void setMotor(float left, float right) {
  leftDuty = left;
  rightDuty = right;

  applyMotor();
}

// Initial setup
void setup() {
  // Turn on LED until board has powered on
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, 0);

  Serial.begin(115200);
  delay(1000); // Power-up safety

  // Look for motors
  Serial.println("looking for motors");
  while (motor1.PRODUCT_ID != PRODUCT_ID_I2C_MOTOR) //wait motor shield ready.
    motor1.getInfo();
  Serial.println("motor shield one up");
  while (motor2.PRODUCT_ID != PRODUCT_ID_I2C_MOTOR) //wait motor shield ready.
    motor2.getInfo();
  Serial.println("motor shield two up");

  // Initialize motors
  motor1.changeFreq(MOTOR_CH_BOTH, 2000);
  motor2.changeFreq(MOTOR_CH_BOTH, 2000);
  motor1.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);
  motor1.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CW);
  motor2.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CW);
  motor2.changeStatus(MOTOR_CH_B, MOTOR_STATUS_CCW);
  motor1.changeDuty(MOTOR_CH_BOTH, 0);
  motor2.changeDuty(MOTOR_CH_BOTH, 0);

  // Connect to Wifi
  WiFi.hostname("GGG car");
  WiFi.begin(ssid, pass);
  udp.begin(PORT);

  Serial.println("Connecting...");
  while (WiFi.status() != WL_CONNECTED)
    delay(500);
  Serial.print("Wifi connected, IP: ");
  Serial.println(WiFi.localIP());

  initRFID();

  if (tcs.begin()) {
    Serial.println("Found color sensor");
    //tcs.setInterrupt(true);
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }

  setupGyro();

  // Turn off LED
  digitalWrite(LED_BUILTIN, 1);
}

// Continuously running code
void loop() {
  long t = millis();
  if (t - loopTime < 50)
    delay(50 - (t - loopTime)); // Make sure there's at least a 50ms interval between loops
  loopTime = millis();


  // -- Networking --
  int packetSize = udp.parsePacket();
  if (packetSize) {
    int len;
    do {
      len = udp.read(incomingPacket, 255);
    } while (packetSize = udp.parsePacket()); // We only really care about the last packet being received

    if (len > 0) {
      // Packets have their type at the very end
      byte type = incomingPacket[len - 1];
      if (type == 1) { // Change follow status
        bool prev = followsColor;
        // If color following status changes, stop the motors
        if (prev != (followsColor = incomingPacket[len - 2]))
          setMotor(0, 0);

        if (followsColor) {
          memcpy(&turnZ, incomingPacket + len - 6, 4);
          memcpy(&turnTime, incomingPacket + len - 10, 4);
          memcpy(&carSpeed, incomingPacket + len - 14, 4);

          // Floats are sent by multiplying by 100 and rounding,
          //  because we didn't feel like playing with genuine binary representations of floats
          int sens;
          memcpy(&sens, incomingPacket + len - 18, 4);
          actionColorSensitivity = sens / 100.0;

          // First read into a short array, then turn these shorts into floats and divide by 100
          uint16_t data[sizeof(actionColors) / sizeof(float)];
          memcpy(&data, incomingPacket + len - sizeof(data) - 18, sizeof(data));
          for (int clr = 0; clr < colorCount; ++clr)
            for (int x = 0; x < 2; ++x)
              actionColors[clr][x] = data[clr * 2 + x] / 100.0;
        }
      } else if (type == 2) { // Request sensor red/green/blue
        uint16_t data[3] = { sRed, sGreen, sBlue };
        
        udp.beginPacket(udp.remoteIP(), udp.remotePort());
        udp.write(2);
        udp.write((byte*) data, sizeof(data));
        udp.endPacket();
      } else if (!followsColor) { // Apply motor data
        int left, right;
        memcpy(&left, incomingPacket + len - 9, sizeof(int));
        memcpy(&right, incomingPacket + len - 5, sizeof(int));
        setMotor(left, right);
      }
    }
  }

  
  // Scan RGB
  uint16_t c; // Unused value

  tcs.getRawData(&sRed, &sGreen, &sBlue, &c);

  if (followsColor) {
    // -- Color following --
    

    int prevColor = activeColor;

    // Find closest color match
    int closest = -1;
    float closestDistance = 0;
    
    if ((sRed != 0 && sRed != 65535) || (sGreen != 0 && sGreen != 65535) || (sBlue != 0 && sBlue != 65535)) {
      float lRatio = (float) sGreen / sRed, rRatio = (float) sBlue / sGreen;
      for (int i = 0; i < colorCount; ++i) {
        float *clr = actionColors[i];
        float lDiff = lRatio - clr[0], rDiff = rRatio - clr[1];
        // abs() is really weird and just seems to give 0.00 (maybe it only works on ints?), so manually doing this
        float dist = (lDiff < 0 ? -lDiff : lDiff) + (rDiff < 0 ? -rDiff : rDiff);

        if (dist < actionColorSensitivity && (closest == -1 || dist < closestDistance)) {
          closest = i;
          closestDistance = dist;
        }
      }
    }

    // More often than not the sensor gives different incorrect readings,
    //  so we only change active color when we've scanned the same color twice
    int prevCounter = colorCounter;
    if (closest == scannedColor)
      ++colorCounter;
    else
      colorCounter = 0;
      
    scannedColor = closest;

    if (colorCounter == 1)
      activeColor = closest;

    int z = readGyroZ();
    
    bool canScan = true;

    // If we're turning, keep turning until we've exceeded a specific Z rotation
    if (turnEnd != 0) {
      if (abs(z) >= turnZ) {
        if (turnDirection == 2)
          setMotor(rightDuty, rightDuty);
        else
          setMotor(leftDuty, leftDuty);
        turnEnd = 0;
        turnDirection = -1;
      } else {
        canScan = false; // If we're still turning, we don't allow scanning of new colors yet
      }
    }

    if (activeColor != -1) {
      // Apply motor duty if we found a match
      if (canScan) {
        int max;
        switch (closest) {
          case 0: // Forward
            setMotor(carSpeed, carSpeed);
            break;
          case 1: // Backward
            setMotor(-carSpeed, -carSpeed);
            break;
          case 4: // Stop
            setMotor(0, 0);
        }
      }
    } else {
      // If we've turned for too long, stop doing it.
      if (turnEnd != 0) {
        long time = millis();
        if (time >= turnEnd) {
          turnEnd = 0;
          if (turnDirection == 2)
            setMotor(rightDuty, rightDuty);
          else
            setMotor(leftDuty, leftDuty);
          turnDirection = -1;
        } else {
          canScan = false;
        }
      }
    }

    // Turning starts as soon as we're no longer scanning a turning color
    if (prevColor != -1 && activeColor == -1) {
      switch (prevColor) {
        case 2: // Left
          setMotor(-leftDuty, leftDuty);
          turnEnd = millis() + turnTime;
          turnDirection = prevColor;
          resetGyroZ();
          break;
        case 3: // Right
          setMotor(leftDuty, -leftDuty);
          turnEnd = millis() + turnTime;
          turnDirection = prevColor;
          resetGyroZ();
          break;
      }
    }
  } else {
    // -- RFID effect handling --
    
    
    // Read RFID tag ID, and apply corresponding effects accordingly
    int tag = readRFIDTag();
    if (tag > 0) {
      effectEnd = millis() + EFFECT_TIME;
      lastTimeLeft = EFFECT_TIME;
      // OIL, REVERSE, GLUE, SUGAR, SLIME
      switch (tag) {
        case 1:
          activeEffect = OIL;
          oilAcc = random(1, 100000);
          break;
        case 2:
          activeEffect = REVERSE;
          break;
        case 5:
          activeEffect = GLUE;
          break;
        case 6:
          activeEffect = SUGAR;
          break;
        case 7:
          activeEffect = SLIME;
          break;
        /* We had two more tags with, but couldn't think of anything for them.
        case 8:
          // TODO
          break;
        case 9:
          // TODO
          break;*/
      }
      applyMotor(); // Don't change duty, but do apply motor so effects are applied
    }
  }

  // If there's an active effect and the time has expired, remove the effect
  if (activeEffect != NO_EFFECT) {
    long timeLeft = effectEnd - millis();
    if (timeLeft <= 0) {
      activeEffect = NO_EFFECT;
      applyMotor();
    } else if (activeEffect == OIL) {
      // The oil effect randomizes the motor a little every 1/4 second.
      if (timeLeft / 250 < lastTimeLeft / 250)
        applyMotor();
    }
    lastTimeLeft = timeLeft;
  }
}
