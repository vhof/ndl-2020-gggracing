#!/usr/bin/python
from queue import Queue
import os
import select
import time

# Button values
KEY_A = 0
KEY_B = 1
KEY_X = 2
KEY_Y = 3
KEY_LB = 4
KEY_RB = 5
KEY_SELECT = 6
KEY_START = 7
KEY_LS = 8
KEY_RS = 9

STICK_LX = 0
STICK_LY = 1
STICK_LT = 2
STICK_RX = 3
STICK_RY = 4
STICK_RT = 5
STICK_DX = 6
STICK_DY = 7

_gamepad_running = True
_gamepad_input = None

_action_queue = Queue()

# States for keys and sticks
keyState = [False] * 10
stickState = [0.0] * 8

# A function that reads an action from the controller,
#  blocking until something is available
def read_gamepad_action():
  try:
    return _action_queue.get()
  except KeyboardInterrupt:
    return None

# Stops the gamepad thread
def gamepad_close():
  global _gamepad_running
  
  _gamepad_running = False
  _gamepad_input.close()

# Gamepad thread function
def gamepad_thread():
  global _gamepad_running
  global _gamepad_input
  
  # Options
  jsFile = "/dev/input/js0"
  
  stickState[STICK_LT] = -1.0
  stickState[STICK_RT] = -1.0
  
  failed = False
  while _gamepad_running:
    # Wait for gamepad to be connected
    print("Awaiting gamepad connection...")
    while _gamepad_running:
      try:
        _gamepad_input = open(jsFile, mode='rb', buffering = -1)
        break
      except BaseException as e:
        time.sleep(1)
        pass
    
    print("Gamepad connected")
    
    while _gamepad_running:
      # A non-blocking way to read data from the file
      try:
        r, w, e = select.select([ _gamepad_input ], [], [], 0)
        if _gamepad_input in r:
          buf = os.read(_gamepad_input.fileno(), 8)
        else:
          continue
      except ValueError:
        continue
      except OSError:
        _gamepad_running = False
      
      if buf == '':
        break
      
      # Parse message
      timestamp = buf[0] | buf[1] << 8 | buf[2] << 16 | buf[3] << 16
      value = buf[4] | buf[5] << 8
      
      group = buf[6] & 0xF
      type = buf[7] & 0xF
      
      # Skip this, we don't want this
      if buf[6] & 0x80:
        continue
      
      if value > 32768: # Poor-man's signed numbers
        value -= 65536
      
      if group == 1: # Keys are either pressed or not pressed
        keyState[type] = value == 1
      elif group == 2: # Stick values [-32767, 32767] should be in range [-1.0, 1.0]
        stickState[type] = value / 32767
      
      _action_queue.put([group == 2, type, value])
    
    print("Gamepad disconnected")
    
    _gamepad_input.close()

if __name__ == "__main__":
  gamepad_thread()
